﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace WolfATG
{
	class Program
	{
		static Bot bot = new Bot();
		static void Main(string[] args)
		{

			var client = bot.Get();

			client.Result.OnMessage += Bot_OnMessage;
			client.Result.StartReceiving();
			Console.ReadKey();
		}

		private static async void Bot_OnMessage(object sender, MessageEventArgs e)
		{
			var commands = Bot.Commands;
			var message = e.Message;
			var botClient = await bot.Get();


			// Проверяет, текст сообщения является ли командой
			foreach (var command in commands)
			{
				if (command.Contains(message.Text))
				{
					command.Execute(message, botClient);
					return;
				}
			}

			//Преобразование сообщения в URL формат

			var strReq = "";

			foreach (var m in message.Text)
			{
				switch (m)
				{
					case ' ':
						strReq += '+';
						break;
					case '/':
						strReq += "\\/";
						break;
					case '=':
						strReq += "%3D";
						break;
					case '+':
						strReq += "%2B";
						break;
					case '^':
						strReq += "%5E";
						break;
					case '%':
						strReq += "%25";
						break;
					default:
						strReq += m;
						break;
				}
			}

			//Запрос

			var httpClient = new HttpClient();
			HttpResponseMessage response = null;
			response = await httpClient.GetAsync(AppSettings.WolfRequest + strReq + AppSettings.WolfRequestParams);

			var content = await response.Content.ReadAsStreamAsync();
			var result = await JsonSerializer.DeserializeAsync<DailyRate>(content);

			if (result.queryresult.pods == null)
			{
				await botClient.SendTextMessageAsync(message.Chat.Id, "Нет результатов.\n Необходимо вводить запрос латинскими буквами.");
				return;
			}

			foreach (var pod in result.queryresult.pods)
			{
				switch (pod.title)
				{
					case "Result":
						await botClient.SendTextMessageAsync(message.Chat.Id, "Результат:");
						break;
					case "Input":
						await botClient.SendTextMessageAsync(message.Chat.Id, "Вы ввели:");
						break;
					case "Number line":
						await botClient.SendTextMessageAsync(message.Chat.Id, "График:");
						break;
					case "Visual representation":
					case "Number name":
						continue;
				}
				// Вывод картинок с результатами
				try
				{
					await botClient.SendPhotoAsync(message.Chat.Id, new InputMedia(pod.subpods[0].img.src));
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception);
				}
				
			}
			

		}
	}

	// Классы для парсера JSON ответа
	class DailyRate
	{
		public QueryRes queryresult { get; set; }
	}

	class QueryRes
	{
		public ObservableCollection<Pod> pods { get; set; }
	}

	class Pod
	{
		public ObservableCollection<SubPod> subpods { get; set; }
		public string title { get; set; }
	}

	class SubPod
	{
		public Imgs img { get; set; }
	}

	class Imgs
	{
		public string src { get; set; }
	}
}
