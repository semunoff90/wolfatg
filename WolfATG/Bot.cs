﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace WolfATG
{
	public class Bot
	{
		private static TelegramBotClient _client;
		private static List<Command> _commandsList;

		public static IReadOnlyList<Command> Commands
		{
			get => _commandsList.AsReadOnly();
		}

		public async Task<TelegramBotClient> Get()
		{
			if (_client != null)
			{
				return _client;
			}

			_commandsList = new List<Command>();
			_commandsList.Add(new CommandHello());
			//Инициализация команд

			_client = new TelegramBotClient(AppSettings.Key);

			return _client;
		}
	}
}
