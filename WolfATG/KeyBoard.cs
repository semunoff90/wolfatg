﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace WolfATG
{
	class KeyBoard
	{
		public KeyBoard(int column, string[] buttonArray, string msg)
		{
			this.msg = msg;
			List<KeyboardButton> ButtonList = new List<KeyboardButton>();
			foreach (var s in buttonArray)
			{
				ButtonList.Add(new KeyboardButton(s));
			}
			this.CountColumn = column;
			this.CountRow = (int)Math.Ceiling((double) (buttonArray.Length / CountColumn));
			this.Buttons = new List<List<KeyboardButton>>();
			for (int i = 0; i < CountRow; i++)
			{
				Buttons.Add(new List<KeyboardButton>());
				for (int j = 0; j < CountColumn; j++)
				{
					this.Buttons[i].Add(ButtonList.First());
					ButtonList.RemoveAt(0);
				}
			}
			this.KeyBoardMark = new ReplyKeyboardMarkup(this.Buttons, true);
		}

		private string msg;
		private int CountColumn;
		private int CountRow;
		private List<List<KeyboardButton>> Buttons;
		private ReplyKeyboardMarkup KeyBoardMark { get; }

		public async Task Show(TelegramBotClient client, long chat_id)
		{
			await client.SendTextMessageAsync(chat_id, this.msg, replyMarkup: this.KeyBoardMark);
		}

		public async Task Hide(TelegramBotClient client, long chat_id)
		{
			await client.SendTextMessageAsync(chat_id, msg, replyMarkup: null);
		}
	}
}
